#!/usr/bin/env bash

if [ "$((1 + RANDOM % 100))" -le 50 ]; then
	file1=$1
	file2=$2
else
	file1=$2
	file2=$1
fi

cond=1
while [ $cond -eq 1 ]; do
	ans=$(zenity --info --text "Слепое тестирование" --no-wrap --ok-label "Выйти" --extra-button "Первый файл" --extra-button "Второй файл")
	cond=$?
	if [[ $ans = "Первый файл" ]]; then
		ffplay -nodisp -autoexit "$file1" 2> /dev/null
		echo "Проигран первый файл"
	elif [[ $ans = "Второй файл" ]]; then
		ffplay -nodisp -autoexit "$file2" 2> /dev/null
		echo "Проигран второй файл"
	fi
done

echo -e "Первый файл: $file1\nВторой файл: $file2\n"
