#!/usr/bin/env python

import argparse
import sys
import os
import code

from wand.image import Image, COMPOSITE_OPERATORS
from wand.color import Color
from wand.display import display
from wand.drawing import Drawing


def apply_mask(image, mask):
    with Image(
        width=image.width, height=image.height, background=Color("transparent")
    ) as alpha_image:
        alpha_image.composite_channel("alpha", mask, "copy_alpha", 0, 0)

        image.composite_channel("alpha", alpha_image, "multiply", 0, 0)

        return image


def converter(file):
    with Image(filename=file) as img:
        real = img.clone()
        mask = img.clone()

    mask.crop(left=mask.width // 2)
    mask.negate(True)

    real.crop(right=real.width // 2)
    real.alpha_channel = True
    real.format = "png"

    return apply_mask(real, mask)


def main():
    parser = argparse.ArgumentParser(
        description="Darkness9724's NScripter sprite converter"
    )

    parser.add_argument(
        "File",
        metavar="file",
        type=argparse.FileType("r"),
        nargs="*",
        help="Nscripter bmp file",
    )

    parser.add_argument(
        "-o", "--output", metavar="outdir", type=str, help="output directory"
    )

    args = parser.parse_args()

    for file in args.File:
        if not file.name.endswith(".bmp"):
            print("The given file is not bmp")
            sys.exit(2)

        converter(file.name).save(
            filename=args.output + "/" + os.path.basename(file.name[:-3] + "png")
        )


if __name__ == "__main__":
    main()
