#!/usr/bin/env sh

echo "Darkness9724's NScripter sprite converter"

show_help() {
    echo "Usage: $(basename "$0") [Nscripter bmp file] [output dir]"
    echo "By default output to current dir"
    echo "-h, --help    show this help"
    #echo "-a, --all     convert an entire directory"
}

converter() {
    DIST="$DIR"/$(basename "$file")
    convert -crop 50%x100% "$file" /tmp/bmp2png.bmp
    convert /tmp/bmp2png-0.bmp \( /tmp/bmp2png-1.bmp -negate \) -alpha off -compose CopyOpacity -composite -quality 100 "${DIST%.*}.png"
}

if [ $# -eq 0 ]
then
    echo "No arguments supplied"
    exit 1
fi

if [ $# -eq 1 ]; then DIR="$(pwd)"; else DIR=$2; fi

while :
do
    case "$1" in
        -h | --help)
            show_help
            exit 0
            ;;
        # TODO: implement
        #-a | --all)
        #    file="*.bmp"
        #    converter
        #    exit 0
        #    ;;
        -*)
            echo "Error: Unknown option: $1"
            show_help
            exit 2
            ;;
        *)
            file=$1
            converter
            exit 0
            ;;
    esac
done
